# ml-rest-service-test

![Componentes de Arquitectura](https://gis97g.sn.files.1drv.com/y4mDuy9UZcd6BdG-h6tMFAmAlEd8wGw3qSp60A4q7Oetwxzg5E0LdDhUbiEzhOQI7M8XcX8afI-6KeAxkCF81oNyMT5FP4yNsd_8RtiiV5vcaCqdWpQHrDOqsACp2lvFH2iv7Mki4nXRpNPWw81evMVjJET9rlkQ51Dmhl7XvyiaM4vo3jf-N9f5dHlgdxWDGStJTrbos0cMKaOT5iHLjjjrA/components_ml.png?psid=1)

Este es un repositorio que contiene una aplicación de tipo Api Rest que es parte de una solución con fines de prueba
tecnica.

Ver tambien el [componente frontend](https://gitlab.com/oswaldoorganization/ml-frontend-test "Repo frontEnd")

### Lenguajes & frameworks

- node.js
- javascript (ES6)
- ExpressJs
- Mongoose (ODM)

### Conexiones

- Atlas Cluster MongoDb: Base de datos

### Api version

Versión 1.0

### API host 

Las peticiones pueden ser realizadas a un host de pruebas con la siguiente url-base:
`'https://ml-rest-app.herokuapp.com/'`

### Descripción de acceso al Api

La comunicación se establece mediante autenticación JWT (Json Web Token), donde va firmada cada petición del usuario.
Para lograr la comunicacion con el Api se debe dar de alta el usuario con un email valido y password. Si se realiza una
petición sin un token valido el servico responde con 401 "Unauthorized".

Ver [Api reference](/Api-Reference.md), reemplazar los valores entre '[value]' por los valores reales:



### Docker
#### Constriur una imagen Docker para su uso.

Si cuenta con su propio cluster de base de datos (tipo mongoDb) asigne el string conection a una variable de sesión de su entornno de terminal, de lo contrario omita este paso y la imagen tomará el cluster de pruebas.

`export DB_CONN='[string-conection]'`

Constriur la imagen, con 

`docker build --build-arg DB_CONN -t [image-name]:v0.1 .`

** omita el flag `--build-arg` en caso de no tener propio cluster de base de datos

Correr Docker para probar

`docker run -it -p 3000:3000 --rm [image-name]:v0.1`

Su contenedor ya estará disponible para solicitudes.

