const express = require('express')
const Audit = require('../models/audit')
const router = new express.Router()

router.post('/audits', async (req, res) => {
  const audit = new Audit(req.body)

  try {
    await audit.save()
    res.status(201).send(audit)
  } catch (e) {
    res.status(400).send(e)
  }
})

router.get('/audits', async (req, res) => {
  try {
    const audits = await Audit.find({})
    res.send(audits)
  } catch (e) {
    res.status(500).send()
  }
})

router.get('/audits/:id', async (req, res) => {
  const _id = req.params.id

  try {
    const audit = await Audit.findById(_id)

    if (!audit) {
      return res.status(404).send()
    }

    res.send(audit)
  } catch (e) {
    res.status(500).send()
  }
})

module.exports = router