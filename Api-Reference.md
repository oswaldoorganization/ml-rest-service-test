- [SingUp](#singup)
- [Sing in](#sing-in)
- [Logout](#logout)
- [Obtener Items](#obtener-items)
- [Obtener Item](#obtener-item)

## SingUp

Request:

<code>POST /users HTTP/1.1 <br>
Host: ml-rest-app.herokuapp.com<br>
Content-Type: application/json<br>

{<br>
"name": "[full name user]",<br>
"email": "[email]",<br>
"password": "[password]"<br>
}</code>

Response: code 201

<code>
{<br>
"user": {<br>
"_id": "[DB-id]",<br>
"name": "[nameUser]",<br>
"email": "[email]",<br>
"__v": 1<br>
},<br>
"token": "[tokenValue]"<br>
}</code>

Si el usuario ya existe responderá con 400 (Bad Request) y un mensaje describiendolo.

## Sing in

Request:

<code>
POST /users/login HTTP/1.1<br>
Host: [base-url]<br>
email: [email]<br>
password: [password]<br>
</code>

Valores en el header

Response code 200:

<code>
{<br>
    "user": {<br>
        "_id": "[db-id]",<br>
        "name": "[name-user]",<br>
        "email": "[email]",<br>
        "__v": [version-doc]<br>
    },
    "token": "[token]"<br>
}
</code>

## Logout

Request:

<code>
POST /users/logout HTTP/1.1<br>
Host: [base-url]<br>
Authorization: Bearer [token]<br>
</code>

Response code 202 (Acepted)

## Obtener Items

Request:

<code>
GET /items/ HTTP/1.1<br>
Host: [base-url]<br>
Authorization: Bearer [token]<br>
</code>


Response code 200:

<code>
{<br>
 "audit": {<br>
        "_id": "[doc-id]",<br>
        "name": "[name-user]",<br>
        "method": "GET",<br>
        "url": "/items/",<br>
        "email": "[email-user]",<br>
        "time": "datetime-request",<br>
        "__v": [version-doc]<br>
    },<br>
"categories": [<br>
        {<br>
            "_id": "[doc-id]",<br>
            "text": "[title]",<br>
            "href": "/items",<br>
            "active": false,<br>
            "__v": [version-doc]<br>
        },<br>
...<br>
],<br>
"items": [<br>
 {<br>
            "price": {<br>
                "currency": "COP",<br>
                "amount": "[amount-item]",<br>
                "decimals": "00"<br>
            },<br>
            "_id": "[doc-id]",<br>
            "id": [id],<br>
            "title": "[title-item]",<br>
            "offer": "[offer-item]",<br>
            "state": "[state-item]",<br>
            "picture": "[image-item]",<br>
            "condition": "[condition-item]",<br>
            "free_shipping": [is-freeshipping],<br>
            "sold_quantity": [quantity],<br>
            "description": "[description-item]",<br>
            "city": "[city-item]",<br>
            "__v": [version-doc]<br>
        },<br>
...<br>
]<br>
}
</code>

## Obtener Item

Request:

[id-item] para efectos de esta prueba use id's 1,2,3,4 para encontrar valores

<code>
GET /items/[id-item] HTTP/1.1<br>
Host: [base-url]<br>
Authorization: Bearer [token]<br>
</code>

Respone code 200:

<code>
{<br>
    "audit": {<br>
        "_id": "[doc-id]",<br>
        "name": "[name-user]",<br>
        "method": "GET",<br>
        "url": "/items/",<br>
        "email": "[email-user]",<br>
        "time": "datetime-request",<br>
        "__v": [version-doc]<br>
    },<br>
    "categories":  [<br>
        {<br>
            "_id": "[doc-id]",<br>
            "text": "[title]",<br>
            "href": "/items",<br>
            "active": false,<br>
            "__v": [version-doc]<br>
        },<br>
...<br>
],<br>,
    "item": {<br>
            "price": {<br>
                "currency": "COP",<br>
                "amount": "[amount-item]",<br>
                "decimals": "00"<br>
            },<br>
            "_id": "[doc-id]",<br>
            "id": [id],<br>
            "title": "[title-item]",<br>
            "offer": "[offer-item]",<br>
            "state": "[state-item]",<br>
            "picture": "[image-item]",<br>
            "condition": "[condition-item]",<br>
            "free_shipping": [is-freeshipping],<br>
            "sold_quantity": [quantity],<br>
            "description": "[description-item]",<br>
            "city": "[city-item]",<br>
            "__v": [version-doc]<br>
        },<br>
}
</code>
