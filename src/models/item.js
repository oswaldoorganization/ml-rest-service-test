const mongoose = require('mongoose')

const Item = mongoose.model('Item', {
  id: {
    type: Number,
    required: true
  },
  title: {
    type: String,
    required: true,
    trim: true
  },
  offer: {
    type: String,
    required: true,
    trim: true
  },
  state: {
    type: String,
    required: true,
    trim: true
  },
  price: {
    currency: String,
    amount: String,
    decimals: String
  },
  picture: String,
  condition: String,
  free_shipping: Boolean,
  sold_quantity: String,
  description: String,
  city: String

})

module.exports = Item