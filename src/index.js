require('./db/mongoose')
require('dotenv').config()
const express = require('express')
const cors = require('cors')
const app = express()
const itemRouter = require('./routers/item')
const auditRouter = require('./routers/audit')
const categoryRouter = require('./routers/category')
const userRouter = require('./routers/user')

const port = process.env.PORT

app.use(express.json())
app.use(cors())

//routes
app.use(itemRouter)
app.use(userRouter)
app.use(auditRouter)
app.use(categoryRouter)
app.use(userRouter)

app.listen(port, () => {
  console.log(`Server is up on port ${port}`)
})