const mongoose = require('mongoose')

const Category = mongoose.model('Category', {

  text: {
    type: String,
    required: true,
    trim: true
  },
  href: {
    type: String,
    required: true,
    trim: true
  },
  active: Boolean
})

module.exports = Category