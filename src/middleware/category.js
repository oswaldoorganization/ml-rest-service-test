const Category = require('./../models/category')

const categories = async (req, res, next) => {
  try {
    const setCategories = await Category.find({})
    res.locals.categories = setCategories
    next()
  } catch (e) {
    res.status(503).send(e)
  }

}

module.exports = {categories}