const Audit = require('./../models/audit')

const audit = async (req, res, next) => {

  const audit = new Audit({
    name: req.user.name,
    method: req.method,
    url: req.url,
    email: req.user.email
  })

  try {
    await audit.save()
    res.locals.audit = audit
    next()
  } catch (e) {
    res.status(503).send(e)
  }
}


module.exports = {audit}