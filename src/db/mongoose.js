const mongoose = require('mongoose')
require('dotenv').config()

const {CLUSTER_MONGODB} = process.env

mongoose.connect(CLUSTER_MONGODB, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false
})