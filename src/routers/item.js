const express = require('express')
const Item = require('../models/item')
const axios = require('axios')
const {audit} = require('../middleware/audit')
const {categories} = require('../middleware/category')
const auth = require('../middleware/auth')
const {formatResult, formatResultItems} = require('./utils/index')
const router = new express.Router()

//configurations axios service with MELI url
const apiMeli = axios.create({
  baseURL: 'https://api.mercadolibre.com',
  timeout: 1000
})

// [Middleware] : This code is executed for every request to the router, example:
// router.use(audit);

router.post('/items', async (req, res) => {
  const item = new Item(req.body)

  try {
    await item.save()
    res.status(201).send(item)
  } catch (e) {
    res.status(400).send(e)
  }
})

router.get('/items', [auth, audit], async (req, res) => {
  const search = req.query.search || ''

  try {
    const result = await apiMeli.get(`/sites/MCO/search?q=${search}`)

    res.send(formatResultItems(res.locals.audit, result))
  } catch (e) {
    res.status(500).send()
  }
})

router.get('/items/:id', [auth, audit], async (req, res) => {
  const id = req.params.id

  try {
    const item = await apiMeli.get(`/items/${id}`)

    if (!item) {
      return res.status(404).send()
    }

    const payloadCategory = await apiMeli.get(`/categories/${item.data.category_id}`)

    const result = formatResult(res.locals.audit, payloadCategory.data)
    result.item = item.data

    res.send(result)
  } catch (e) {
    res.status(500).send()
  }
})

router.get('/items/:id/description', [auth, audit], async (req, res) => {
  const id = req.params.id

  try {
    const result = await apiMeli.get(`/items/${id}/description`)

    if (!result) {
      return res.status(404).send()
    }

    res.send(result.data)
  } catch (e) {
    res.status(e.response.status).send(e.response.data)
  }
})

router.delete('/items/:id', async (req, res) => {
  try {
    const item = await Item.findByIdAndDelete(req.params.id)

    if (!item) {
      res.status(404).send()
    }

    res.send(item)
  } catch (e) {
    res.status(500).send()
  }
})

module.exports = router