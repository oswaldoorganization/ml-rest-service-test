const formatResult = (audit, categories) => {

  return {
    audit: audit ? audit._doc : null,
    categories: categories ? categories['path_from_root'] : null,
  }

}

const formatResultItems = (audit, payload) => {
  let items = [], categories = []

  if (payload.data.results.length) {
    let filters_Category = payload.data.filters.filter(item => item.id == 'category')
    categories = !filters_Category.length ? payload.data.query.split(' ') : obtainCategories(filters_Category)

    items = payload.data.results.slice(0, 4)
  }

  return {
    audit: audit ? audit._doc : null,
    categories,
    items
  }
}

//private
const obtainCategories = (categorySection, catArray) => {

  categorySection[0].values.forEach(v => {
    catArray = v['path_from_root'].map(o => o)
  })

  return catArray
}

module.exports = {formatResult, formatResultItems}