const mongoose = require('mongoose')
const validator = require('validator')

const Audit = mongoose.model('Audit', {
  name: {
    type: String,
    required: true,
    trim: true
  },
  email: {
    type: String,
    required: true,
    trim: true,
    lowercase: true,
    validate (value) {
      if (!validator.isEmail(value)) {
        throw new Error('Email is invalid')
      }
    }
  },
  method: {
    type: String,
    required: true,
    trim: true
  },
  url: {
    type: String,
    required: true,
    trim: true
  },

  time : { type : Date, default: Date.now }
})

module.exports = Audit